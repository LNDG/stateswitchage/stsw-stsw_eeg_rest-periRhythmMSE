# mMSE
Code &amp; short tutorial for modified multi-scale entropy analysis

If you find this code useful, please cite:

Grandy et al (2016): On the estimation of brain signal entropy from sparse neuroimaging data

Kosciessa et al. (2019): Standard multiscale entropy reflects spectral power at mismatched temporal scales

Kloosterman et al. (in prep.): Increased Brain Signal Variability Underlies Strategic Decision Bias Adjustment

-----------

Short tutorial following soon ...

-----------

Note: GitHub directory is currently a unidirectional mirror of a GitLab directory.