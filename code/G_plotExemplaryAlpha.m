% Check the SNR of added rhythmicity

%% set relevant paths

    pn.rootDir      = '/Volumes/LNDG/Projects/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
    pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.NoiseTools   = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
    pn.data         = [pn.rootDir, 'B_data/'];
    pn.plotFolder   = [pn.rootDir, 'C_figures/'];
    
%% load simulated simData

    load([pn.data, 'A0A_frequencyData.mat'], 'simData');
    simDataOrig = simData;

    frequencies = [1, 2.5, 5, 10, 20, 40, 80];
    
%% plot alpha example
    
    indFreq = 4;
    simData.trial = squeeze(simDataOrig.trial(:,indFreq,:))';
    dataCat = cat(3, simData.trial{:});

    time = simData.time{1};
    signal_1f = squeeze(nanmean(dataCat(1,:,5),3));
    signal_alpha = squeeze(nanmean(dataCat(7,:,5),3));
    
    NaNSignal = NaN(size(signal_alpha));
    plot_1f = [signal_1f(1:100), NaNSignal(101:250), signal_1f(251:350)];
    plot_alpha = [NaNSignal(1:100), signal_alpha(101:250), NaNSignal(251:350)];
    
    % plot 500 ms arrhythmic, 1 s rhythmic, 500 ms arrhythmic
    
    h = figure('units','normalized','position',[.1 .1 .2 .1]);
    set(gcf,'renderer','Painters')
    hold on;
    plot(plot_1f, 'k', 'LineWidth', 2)
    plot(plot_alpha, 'r', 'LineWidth', 2)
    axis off
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
    figureName = 'Z_schematicAlphaOnOffset';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
