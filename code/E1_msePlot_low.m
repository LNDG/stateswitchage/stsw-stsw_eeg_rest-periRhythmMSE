
%% set up paths

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/';
pn.dataInOUT  = [pn.root, 'B_data/C_MSE_Output_mdSplit/'];

%% load data

load([pn.dataInOUT, 'mseavg250_CSD.mat'], 'mseavg');

%% plot data for all subjects in same plot

% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/ploterr')
% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools')
% 
% labels = [];
% labels{1} = 'Begin-Pre';
% labels{1} = 'Begin-Pre';
% labels{3} = 'End-Post';
% labels{4} = 'End-Post';
% 
% titles = [];
% titles{1} = 'No Notch';
% titles{2} = 'Notch-filtered';
% titles{3} = 'No Notch';
% titles{4} = 'Notch-filtered';
% 
% dat = [];
% dat{1} = mseavg.dat(1:end,1,1,2)-mseavg.dat(1:end, 1,1,1);
% dat{2} = mseavg.dat(1:end,3,1,2)-mseavg.dat(1:end, 3,1,1);
% dat{3} = mseavg.dat(1:end,2,1,1)-mseavg.dat(1:end, 2,1,2);
% dat{4} = mseavg.dat(1:end,4,1,1)-mseavg.dat(1:end, 4,1,2);
% 
% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/T_tools/winsor') % add function to winsorize data
% 
% for indCond = 1:4
%     dat{indCond} = winsor(dat{indCond}, [2.5 97.5]);
% end
% 
% h = figure('units','normalized','position',[.1 .1 .15*2 .15*2]); clc; hold on;
% 
% for indPlot = 1:4
%     subplot(2,2,indPlot); hold on;
%     % if we want each bar to have a different color, loop
%     bar(1, nanmean(dat{indPlot}), 'FaceColor',  [100/265 150/265 200/265], 'EdgeColor', 'none', 'BarWidth', 0.4);
%     % plot jittered individual values on top
%     scatter(repmat(1,size(dat{indPlot},1),1)+(rand(size(dat{indPlot},1),1)-.5).*.4, dat{indPlot}(:,1), 10, 'filled', 'MarkerFaceColor', [.2 .2 .2]);
%     % show standard deviation on top
%     h1 = ploterr(1, nanmean(dat{indPlot},1), [], nanstd(dat{indPlot},[],1)./sqrt(size(dat{indPlot},1)), 'k.', 'abshhxy', 0);
%     set(h1(1), 'marker', 'none'); % remove marker
%     set(h1(2), 'LineWidth', 4);
% 
%     % label what we're seeing
%     % if labels are too long to fit, use the xticklabelrotation with about -30
%     % to rotate them so they're readable
%     set(gca, 'xtick', [1], 'xticklabel', labels{indPlot}, 'xlim', [0.5 1.5]);% ylim([-.05 .03])
%     ylabel('Sample Entropy'); %xlabel('# of targets');
% 
%     % if these data are paired, show the differences
%     % plot(dat', '.k-', 'linewidth', 0.2, 'markersize', 2);
% 
%     for indCond = 1
%         % significance star for the difference
%         [~, pval] = ttest(dat{indPlot}(:, indCond)); % paired t-test
%         % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%         % sigstars on top
%         mysigstar(gca, [indCond], 0, pval);
% 
%     end
%     title(titles{indPlot})
% end
% set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% add within-subject standard errors & significance bars

% new value = old value - subject average + grand average

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/T_tools/barwitherr');
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/T_tools'))

h = figure('units','normalized','position',[.1 .1 .2 .05]);
subplot(1,2,1); cla; hold on;
    subjAvg = repmat(squeeze(nanmean(nanmean(mseavg.dat(1:end,1:2,1,1:2),4),2)),1,2,1,2);
    grandAvg = repmat(squeeze(nanmean(nanmean(nanmean(mseavg.dat(1:end,1:2,1,1:2),4),2),1)),99,2,1,2);
    curData = mseavg.dat(1:end,1:2,1,1:2) - subjAvg + grandAvg;

    curData = [squeeze(curData(1:end, 1,1,1)), squeeze(curData(1:end, 1,1,2)),...
        squeeze(curData(1:end, 2,1,1)), squeeze(curData(1:end, 2,1,2))];

    origData = [squeeze(mseavg.dat(1:end, 1,1,1)), squeeze(mseavg.dat(1:end, 1,1,2)),...
        squeeze(mseavg.dat(1:end, 2,1,1)), squeeze(mseavg.dat(1:end, 2,1,2))];
    
    condPairs = [1,2; 3,4];
    condPairsLevel = [.48 .48];
    meanY = nanmean(curData,1);
    errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
    %[h1, hError] = barwitherr(errorY, meanY);
    [h1] = bar(1, meanY(1), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(1, meanY([1]), errorY([1]), 'Color', [0 0 0], 'LineWidth', 2);
    [h1] = bar(2, meanY(2), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(2, meanY([2]), errorY([2]), 'Color', [0 0 0],'LineWidth', 2);
    [h1] = bar(3, meanY(3), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(3, meanY([3]), errorY([3]), 'Color', [0 0 0],'LineWidth', 2);
    [h1] = bar(4, meanY(4), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(4, meanY([4]), errorY([4]), 'Color', [0 0 0],'LineWidth', 2);

    %[h1, hError] = barwitherr(2, errorY([2]), meanY([2]));
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(origData(:,condPairs(indPair,1)), origData(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    box(gca,'off')
    set(gca, 'XTick', []);set(gca, 'YTick', [.45 .48]);
    ylabel({'SampEn'})
    ylim([.445 .48]); xlim([.5 4.5])
subplot(1,2,2); cla; hold on;
    subjAvg = repmat(squeeze(nanmean(nanmean(mseavg.dat(1:end,3:4,1,1:2),4),2)),1,2,1,2);
    grandAvg = repmat(squeeze(nanmean(nanmean(nanmean(mseavg.dat(1:end,3:4,1,1:2),4),2),1)),99,2,1,2);
    curData = mseavg.dat(1:end,3:4,1,1:2) - subjAvg + grandAvg;

    curData = [squeeze(curData(1:end, 1,1,1)), squeeze(curData(1:end, 1,1,2)),...
        squeeze(curData(1:end, 2,1,1)), squeeze(curData(1:end, 2,1,2))];

    origData = [squeeze(mseavg.dat(1:end, 3,1,1)), squeeze(mseavg.dat(1:end, 3,1,2)),...
        squeeze(mseavg.dat(1:end, 4,1,1)), squeeze(mseavg.dat(1:end, 4,1,2))];
    
    condPairs = [1,2; 3,4];
    condPairsLevel = [.54 .54];
    meanY = nanmean(curData,1);
    errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
    %[h1, hError] = barwitherr(errorY, meanY);
    [h1] = bar(1, meanY(1), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(1, meanY([1]), errorY([1]), 'Color', [0 0 0], 'LineWidth', 2);
    [h1] = bar(2, meanY(2), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(2, meanY([2]), errorY([2]), 'Color', [0 0 0],'LineWidth', 2);
    [h1] = bar(3, meanY(3), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(3, meanY([3]), errorY([3]), 'Color', [0 0 0],'LineWidth', 2);
    [h1] = bar(4, meanY(4), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(4, meanY([4]), errorY([4]), 'Color', [0 0 0],'LineWidth', 2);

    %[h1, hError] = barwitherr(2, errorY([2]), meanY([2]));
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(origData(:,condPairs(indPair,1)), origData(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    box(gca,'off')
    set(gca, 'XTick', []); set(gca, 'YTick', [.5 .52]);
    ylabel({'SampEn'})
    ylim([.5 .52]); xlim([.5 4.5])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
    figureName = 'E_SampEn_Alpha_Errors_low';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% plot raincloudplots

% new value = old value - subject average + grand average

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/T_tools/RainCloudPlots'))

h = figure('units','normalized','position',[.1 .1 .2 .1]);
set(gcf,'renderer','Painters')
subplot(1,2,1); cla; hold on;

origData = [squeeze(mseavg.dat(1:end, 1,1,1)), squeeze(mseavg.dat(1:end, 1,1,2)),...
        squeeze(mseavg.dat(1:end, 2,1,1)), squeeze(mseavg.dat(1:end, 2,1,2))];
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(origData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = origData(:,i)-...
                nanmean(origData(:,:),2)+...
                repmat(nanmean(nanmean(origData(:,:),2),1),size(origData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = [.2 .2 .2];%cBrew(4,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        condPairs = [1,2; 3,4];
        condPairsLevel = [.50 .50];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    xlabel({'SampEn'})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim([.4 .52]);
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
   
    % calculate dprime: (meanpost-meanpre)/STD(post-pre)
    dprime1 = (mean(data{2,1})-mean(data{1,1}))./std(data{2,1}-data{1,1})
    dprime2 = (mean(data{3,1})-mean(data{4,1}))./std(data{3,1}-data{4,1})
    
    % get p-value
    [~, pval,ci,stats] = ttest(data{2,1}, data{1,1}); % paired t-test
    [~, pval,ci,stats] = ttest(data{3,1}, data{4,1}); % paired t-test

subplot(1,2,2); cla; hold on;

    origData = [squeeze(mseavg.dat(1:end, 3,1,1)), squeeze(mseavg.dat(1:end, 3,1,2)),...
        squeeze(mseavg.dat(1:end, 4,1,1)), squeeze(mseavg.dat(1:end, 4,1,2))];
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(origData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = origData(:,i)-...
                nanmean(origData(:,:),2)+...
                repmat(nanmean(nanmean(origData(:,:),2),1),size(origData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = [.2 .2 .2];%cBrew(4,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        condPairs = [1,2; 3,4];
        condPairsLevel = [.50 .50];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    xlabel({'SampEn'})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim([.43 .57]);
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
   
    % calculate dprime: (meanpost-meanpre)/STD(post-pre)
    dprime1 = (mean(data{2,1})-mean(data{1,1}))./std(data{2,1}-data{1,1})
    dprime2 = (mean(data{3,1})-mean(data{4,1}))./std(data{3,1}-data{4,1})
    
    % get p-value
    [~, pval,ci,stats] = ttest(data{2,1}, data{1,1}); % paired t-test
    [~, pval,ci,stats] = ttest(data{3,1}, data{4,1}); % paired t-test

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
    figureName = 'E_SampEn_Alpha_Errors_low_RCP';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% partial out age & amount of individually detected events

% N = 47 YAs + 52 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

demo = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/demographics/B_data/Questionnaire_STSWD_NoPilot_Clean.mat');

IDs_demo = demo.T.x_id;
Age = demo.Age;

idx_IDs = ismember(cellstr(num2str(IDs_demo)), IDs);

% get amount of available trials

for indID = 1:numel(IDs)
	tmpData = load([pn.root, 'B_data/B_periRhythmOut/', sprintf('%s_PeriAlpha_Freq.mat', IDs{indID})]);
    tmpData = cat(1,tmpData.PeriEpisodeERP.trial_onset{:});
    NumofEvents(indID) = size(tmpData,1); clear tmpData;
end

%% linear mixed models: predicting entropy from condition, covarying age and number of events

% turn into long format

% id, entropy, condition, age, eventNumber

% onset vs pre
dataTable.id = repmat(IDs,2,1); 
dataTable.age = repmat(Age(idx_IDs),2,1);
dataTable.events = repmat(NumofEvents',2,1);
dataTable.sampen = [mseavg.dat(1:end,1,1,1);mseavg.dat(1:end, 1,1,2)];
dataTable.cond = [repmat(1,numel(IDs),1); repmat(2,numel(IDs),1)];

tbl = table(categorical(dataTable.cond),double(dataTable.sampen),...
    double(dataTable.age), double(dataTable.events),categorical(dataTable.id),...
    'VariableNames',{'Condition','sampen','age', 'events', 'Subject'});

lme1 = fitlme(tbl,'sampen ~ Condition + (1 | Subject)');
%    Name                 Estimate       SE            tStat      DF     pValue        Lower          Upper      
%    'Condition_2'        -0.030537    0.0019121     15.97    196    2.5373e-37    0.026766    0.034308
lme2 = fitlme(tbl,'sampen ~ age*Condition + events*Condition + age*events + (1 | Subject)');
%    'Condition_2'        -0.027113     0.0072106        3.7601    191    0.00022571        0.01289       0.041335

% offset vs. post
dataTable.id = repmat(IDs,2,1); 
dataTable.age = repmat(Age(idx_IDs),2,1);
dataTable.events = repmat(NumofEvents',2,1);
dataTable.sampen = [mseavg.dat(1:end,2,1,2); mseavg.dat(1:end, 2,1,1)];
dataTable.cond = [repmat(1,numel(IDs),1); repmat(2,numel(IDs),1)];

tbl = table(categorical(dataTable.cond),double(dataTable.sampen),...
    double(dataTable.age), double(dataTable.events),categorical(dataTable.id),...
    'VariableNames',{'Condition','sampen','age', 'events', 'Subject'});

lme1 = fitlme(tbl,'sampen ~ Condition + (1 | Subject)');
%    'Condition_2'        -0.035756    0.0024288    -14.721    196    1.5843e-33    -0.040546    -0.030966
lme2 = fitlme(tbl,'sampen ~ age*Condition + events*Condition + age*events + (1 | Subject)');
%    'Condition_2'        -0.029479     0.0090687     -3.2506    191     0.0013609      -0.047367      -0.011591

% onset vs. pre: notched
dataTable.id = repmat(IDs,2,1); 
dataTable.age = repmat(Age(idx_IDs),2,1);
dataTable.events = repmat(NumofEvents',2,1);
dataTable.sampen = [mseavg.dat(1:end,3,1,1); mseavg.dat(1:end, 3,1,2)];
dataTable.cond = [repmat(1,numel(IDs),1); repmat(2,numel(IDs),1)];

tbl = table(categorical(dataTable.cond),double(dataTable.sampen),...
    double(dataTable.age), double(dataTable.events),categorical(dataTable.id),...
    'VariableNames',{'Condition','sampen','age', 'events', 'Subject'});

lme1 = fitlme(tbl,'sampen ~ Condition + (1 | Subject)');
%    'Condition_2'        -0.0058116    0.0017584    3.3051    196      0.0011289    0.0023438    0.0092793
lme2 = fitlme(tbl,'sampen ~ age*Condition + events*Condition + age*events + (1 | Subject)');
%    'Condition_2'                -0.0068576     0.0065504      1.0469    191       0.29646     -0.0060627      0.019778

lme2 = fitlme(tbl,'sampen ~ age + events + Condition + (1 | Subject)');
%    Name                 Estimate       SE            tStat      DF     pValue        Lower          Upper      
% 'Condition_2'         -0.0058116     0.0017584    -3.3051    194     0.0011309     -0.0092795     -0.0023436
lme2 = fitlme(tbl,'sampen ~ age*Condition + events + Condition + (1 | Subject)');
% 'Condition_2'         -0.00083636     0.0042258    -0.19792    193       0.84332     -0.0091711      0.0074984
%'Condition_2:age'      -0.00010294    7.9641e-05     -1.2925    193       0.19773    -0.00026001     5.4143e-05
lme2 = fitlme(tbl,'sampen ~ age + Condition*events + Condition + (1 | Subject)');

% offset vs. post: notched
dataTable.id = repmat(IDs,2,1); 
dataTable.age = repmat(Age(idx_IDs),2,1);
dataTable.events = repmat(NumofEvents',2,1);
dataTable.sampen = [mseavg.dat(1:end,4,1,2); mseavg.dat(1:end, 4,1,1)];
dataTable.cond = [repmat(1,numel(IDs),1); repmat(2,numel(IDs),1)];

tbl = table(categorical(dataTable.cond),double(dataTable.sampen),...
    double(dataTable.age), double(dataTable.events),categorical(dataTable.id),...
    'VariableNames',{'Condition','sampen','age', 'events', 'Subject'});

lme1 = fitlme(tbl,'sampen ~ Condition + (1 | Subject)');
stats = anova(lme1)
%    Condition_2'        -0.0066342    0.0019919    -3.3306    196      0.0010355    -0.010562    -0.0027059
lme2 = fitlme(tbl,'sampen ~ age*Condition + events*Condition + age*events + (1 | Subject)');
%    'Condition_2'       -0.014575     0.0073682      -1.978    191      0.049363      -0.029108    -4.1069e-05


