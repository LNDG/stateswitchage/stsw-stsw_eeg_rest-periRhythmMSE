% Collect individual results

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/';
pn.dataOut      = [pn.root, 'F_periRhythmMSE/B_data/B_periRhythmOut/']; 

% N = 47 YAs + 52 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

PeriEpisodeERPMerged_trial_onset = [];
PeriEpisodeERPMerged_trial_offset = [];
PeriEpisodeERPMerged_trial_onset_notch = [];
PeriEpisodeERPMerged_trial_offset_notch = [];

for indID = 1:numel(IDs)
   load([pn.dataOut, IDs{indID}, '_PeriAlpha_Freq.mat'], 'PeriEpisodeERP')
   PeriEpisodeERPMerged_trial_onset = cat(1, PeriEpisodeERPMerged_trial_onset, cat(1,PeriEpisodeERP.trial_onset{:}));
   PeriEpisodeERPMerged_trial_offset = cat(1, PeriEpisodeERPMerged_trial_offset, cat(1,PeriEpisodeERP.trial_offset{:}));
   PeriEpisodeERPMerged_trial_onset_notch = cat(1, PeriEpisodeERPMerged_trial_onset_notch, cat(1,PeriEpisodeERP.trial_onset_notch{:}));
   PeriEpisodeERPMerged_trial_offset_notch = cat(1, PeriEpisodeERPMerged_trial_offset_notch, cat(1,PeriEpisodeERP.trial_offset_notch{:}));
end

figure; 
subplot(2,2,1); imagesc(zscore(squeeze(PeriEpisodeERPMerged_trial_onset(:,:)),[],2),[-3 3])
subplot(2,2,2); imagesc(zscore(squeeze(PeriEpisodeERPMerged_trial_offset(:,:)),[],2),[-3 3])
subplot(2,2,3); imagesc(zscore(squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)),[],2),[-3 3])
subplot(2,2,4); imagesc(zscore(squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)),[],2),[-3 3])
colormap('gray')

%% sort according to instantaneous phase

smoothIndex = 1;
%clim = [-5*10^-4, 5*10^-4];
clim = [-2 2];
clim = [-.5 .5];

sortAtSample_onset = 300;
alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
alphaFiltered_onset = zscore(alphaFiltered_onset, [], 2);
y = hilbert(alphaFiltered_onset); sigphase = angle(y); [~, sortInd_onset] = sort(sigphase(:,sortAtSample_onset), 'ascend');

sortAtSample_offset = 200;
alphaFiltered_offset = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); 
alphaFiltered_offset = zscore(alphaFiltered_offset, [], 2);
y = hilbert(alphaFiltered_offset); sigphase = angle(y); [~, sortInd_offset] = sort(sigphase(:,sortAtSample_offset), 'ascend');

figure; 
subplot(3,2,1);
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:)); x = zscore(x, [], 2);
    xForFilter = x; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth, size(xForFilter))'; x_smooth = x_smooth';
    imagesc(x_smooth(sortInd_onset,:), clim)
    title('Transition to alpha onset')
subplot(3,2,2);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_offset,:), clim)
    title('Transition to alpha offset')
subplot(3,2,3);
    x = squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_onset,:), clim)
subplot(3,2,4);
    x = squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_offset,:), clim)
subplot(3,2,5);
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_onset,:), [-.05 .05])
subplot(3,2,6);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_offset,:), [-.2 .2])
colormap('gray')

%% sort according to instantaneous phase (use R2017 smoothing)

smoothIndex = 1;
%clim = [-5*10^-4, 5*10^-4];
clim = [-.2 .2];

% sortAtSample_onset = 300;
% alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
% alphaFiltered_onset = zscore(alphaFiltered_onset, [], 2);
% alphaFiltered_onset = smoothdata(alphaFiltered_onset,2,'movmean', 50);
% y = hilbert(alphaFiltered_onset); sigphase = angle(y); [~, sortInd_onset] = sort(sigphase(:,sortAtSample_onset), 'ascend');
% 
% sortAtSample_offset = 200;
% alphaFiltered_offset = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); 
% alphaFiltered_offset = zscore(alphaFiltered_offset, [], 2);
% alphaFiltered_offset = smoothdata(alphaFiltered_offset,2,'movmean', 50);
% y = hilbert(alphaFiltered_offset); sigphase = angle(y); [~, sortInd_offset] = sort(sigphase(:,sortAtSample_offset), 'ascend');

sortAtSample_onset = 300;
alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
%alphaFiltered_onset = zscore(alphaFiltered_onset, [], 2);
y = hilbert(alphaFiltered_onset); sigphase = angle(y); [~, sortInd_onset] = sort(sigphase(:,sortAtSample_onset), 'ascend');

sortAtSample_offset = 200;
alphaFiltered_offset = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); 
%alphaFiltered_offset = zscore(alphaFiltered_offset, [], 2);
y = hilbert(alphaFiltered_offset); sigphase = angle(y); [~, sortInd_offset] = sort(sigphase(:,sortAtSample_offset), 'ascend');


figure; 
subplot(3,2,1);
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', 1);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_onset,:), clim)
    title('Transition to alpha onset')
subplot(3,2,2);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:)); x = zscore(x, [], 2);
    %x_smooth = smoothdata(x,2,'movmean', smoothIndex);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_offset,:), clim)
    title('Transition to alpha offset')
subplot(3,2,3);
    x = squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_onset,:), clim)
subplot(3,2,4);
    x = squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_offset,:), clim)
subplot(3,2,5);
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,2,'movmean', 50);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_onset,:), clim)
subplot(3,2,6);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,2,'movmean', 50);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_offset,:), clim)
colormap('gray')

%% align signals to local minimum around indicated time

alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
alphaFiltered_onset = zscore(alphaFiltered_onset, [], 2);

localMimimaOnset = islocalmin(alphaFiltered_onset(:,250:275),2);
localMimimaOnset = localMimimaOnset & cumsum(localMimimaOnset,2) == 1; % get first element in each row
[A, B] = find(localMimimaOnset);
[~, sortInd] = sort(A, 'ascend'); 
Onset_minimum = 250+B(sortInd)-1;

PeriEpisodeERPMerged_trial_onset_shifted = PeriEpisodeERPMerged_trial_onset(:,Onset_minimum-200:Onset_minimum+200);

% figure; cla; hold on; plot(PeriEpisodeERPMerged_trial_onset(9,:)); plot(PeriEpisodeERPMerged_trial_onset(8,:)); 
% figure; cla; hold on; plot(PeriEpisodeERPMerged_trial_onset_notch(9,:)); plot(PeriEpisodeERPMerged_trial_onset_notch(8,:)); 
% 


time = -250/500:1/500:250/500;
figure; cla; hold on; 
patches.timeVec = [0 .5];
patches.colorVec = [1 .95 .8];
for indP = 1:size(patches.timeVec,2)-1
    YLim = [0 500];
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
for indTrial = 1:30
    curTrial = randperm(size(PeriEpisodeERPMerged_trial_offset,1));
    curTrial = curTrial(1);
    plot(time, zscore(PeriEpisodeERPMerged_trial_onset(curTrial,:))+indTrial*4, 'k');
    %plot(zscore(PeriEpisodeERPMerged_trial_onset_notch(indTrial,:))+indTrial*3, 'r');
end
ylim([0 130]); xlabel('Time from indicated alpha onset [s]')
title('Exemplary alpha onset traces')


time = -250/500:1/500:250/500;
figure; cla; hold on; 
patches.timeVec = [-.5 0];
patches.colorVec = [1 .95 .8];
for indP = 1:size(patches.timeVec,2)-1
    YLim = [0 500];
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
for indTrial = 1:30
    curTrial = randperm(size(PeriEpisodeERPMerged_trial_offset,1));
    curTrial = curTrial(1);
    plot(time, zscore(PeriEpisodeERPMerged_trial_offset(curTrial,:))+indTrial*4, 'k');
    %plot(zscore(PeriEpisodeERPMerged_trial_onset_notch(curTrial,:))+indTrial*4, 'r');
end
ylim([0 130]); xlabel('Time from indicated alpha offset [s]')
title('Exemplary alpha offset traces')

figure; 
    x = squeeze(PeriEpisodeERPMerged_trial_onset_shifted(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', 20);
    imagesc(x_smooth(:,:), clim)

    
%% plot for different subjects

for indID = 1:numel(IDs)
   load([pn.dataOut, IDs{indID}, '_PeriAlpha_Freq.mat'], 'PeriEpisodeERP')
   curData_onset = cat(1,PeriEpisodeERP.trial_onset{:});

    time = -250/500:1/500:250/500;
    h = figure('units','normalized','position',[.1 .1 .25 .3]);
    cla; hold on; 
    patches.timeVec = [0 .5];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [0 500];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    for indTrial = 1:30
        curTrial = randperm(size(curData_onset,1));
        curTrial = curTrial(1);
        plot(time, zscore(curData_onset(curTrial,:))+indTrial*4, 'k');
        %plot(zscore(PeriEpisodeERPMerged_trial_onset_notch(indTrial,:))+indTrial*3, 'r');
    end
    ylim([0 130]); xlabel('Time from indicated alpha onset [s]')
    title(['Exemplary alpha onset traces: ', IDs{indID}])

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/A_IndividualAlpha/';
    figureName = ['AlphaOnset_', IDs{indID}];
    saveas(h, [pn.plotFolder, figureName], 'png');
    close(h);
end
