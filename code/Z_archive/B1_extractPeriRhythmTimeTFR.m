function A_extractPeriRhythmTimeTFR

% Note: channels refer to a pair of 'rhythm detected' & the TFR at that
% channel. As the onsets may vary between channels, this means that the TFR
% content may also temporally vary across channels.

%% set up paths

% % convert from UNIX input
% indID = str2num(indID);
% indFreq = str2num(indFreq);

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/';
pn.preprocData  = [pn.root, 'D_eBOSC_noDur/B_data/A_dataPreBOSC/'];
pn.dataBOSC     = [pn.root, 'D_eBOSC_noDur/B_data/B_eBOSCout/'];
pn.dataIn      = [pn.root, 'F_periRhythmMSE/B_data/A_eventTiming/'];
pn.dataOut      = [pn.root, 'F_periRhythmMSE/B_data/B_periRhythmOut/'];
addpath([pn.root, 'F_periRhythmMSE/T_tools//']);
addpath([pn.root, 'F_periRhythmMSE/T_tools/fieldtrip-20170904']); ft_defaults;

% N = 47 YAs + 52 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for indID = 1:numel(IDs)

    %% load preproc & eBOSC data

    load([pn.preprocData, IDs{indID}, '_eBOSCinput.mat']); % load data (only one needed for time indication)
    load([pn.dataIn, IDs{indID}, '_EpisodeTiming.mat'])
  
    cond = {'EO'};
    indCond = 1;
    
    %% notch-filter the 8-15 Hz alpha band
    
    cfg_hpf.bsfilter = 'yes';
    cfg_hpf.bsfreq = [8 15];
    cfg_hpf.bsfiltord = 6;
    [data_HPF.(cond{indCond})] = ft_preprocessing(cfg_hpf, data.(cond{indCond}));
 
%% create TFR & encode rhythm-dependent results

    freqRanges = [8, 15];
    
    for indCond = 1
        
        % initiate output structure
        PeriEpisodeERP = data.EO;
        PeriEpisodeERP.trial = [];
        PeriEpisodeERP.trial_onset = [];
        PeriEpisodeERP.trial_offset = [];
        PeriEpisodeERP.trial_onset_notch = [];
        PeriEpisodeERP.trial_offset_notch = [];
       
        for indRhythmChan = 58:60 % this is the index for the channel at which rhythmicity is detected        
            
            curOnsets = []; curOnsets = EventOnset_s{indRhythmChan};
            curOffsets = []; curOffsets = EventOffset_s{indRhythmChan};
            
            % if the distance between offset and subsequent event is less
            % than 500 ms, fuse events together
            
            idxToFuse = NaN;
            while ~isempty(idxToFuse)
                idxToFuse = find(curOffsets(2:end)-curOnsets(1:end-1)<0.5);
                curOffsets(idxToFuse) = curOffsets(idxToFuse+1);
                curOnsets(idxToFuse) = [];
                curOffsets(idxToFuse) = [];
            end
            
             %% plot superimposed detected periods
             
             [~, tmp_onset] = min(abs(curOnsets'-repmat(time,numel(curOnsets),1)),[],2);
             [~, tmp_offset] = min(abs(curOffsets'-repmat(time,numel(curOnsets),1)),[],2);
                          
            time = data.(cond{indCond}).time{1};
            figure; cla; hold on; 
            patches.timeVec = [curOnsets', curOffsets'];
            patches.colorVec = [1 .95 .8];
            for indP = 1:size(patches.timeVec,1)-1
                YLim = [-10 10];
                p = patch([patches.timeVec(indP,1) patches.timeVec(indP,2) patches.timeVec(indP,2) patches.timeVec(indP,1)], ...
                            [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(1,:));
                p.EdgeColor = 'none';
            end
            plot(time, zscore(nanmean(data.(cond{indCond}).trial{tmp_trial}(58:60,:),1)), 'k', 'LineWidth', 2);

             %% extract detected periods
             
            if not(isempty(curOnsets))
                for indEp = 1:numel(curOnsets)
            
                    time = data.EO.time{1};
                    tmp_onset = find(time >= curOnsets(indEp), 1, 'first');
                    tmp_offset = find(time <= curOffsets(indEp), 1, 'last');
                    tmp_trial = 1;

                    % -500ms:+500ms surrounding rhythmic episode
                    timePointsOnset = tmp_onset-250:tmp_onset+250;
                    timePointsOffset = tmp_offset-250:tmp_offset+250;
                    
                    PeriEpisodeERP.trial_onset{indRhythmChan}(indEp,:) = data.(cond{indCond}).trial{tmp_trial}(indRhythmChan,timePointsOnset);
                    PeriEpisodeERP.trial_offset{indRhythmChan}(indEp,:) = data.(cond{indCond}).trial{tmp_trial}(indRhythmChan,timePointsOffset);
                    PeriEpisodeERP.trial_onset_notch{indRhythmChan}(indEp,:) = data_HPF.(cond{indCond}).trial{tmp_trial}(indRhythmChan,timePointsOnset);
                    PeriEpisodeERP.trial_offset_notch{indRhythmChan}(indEp,:) = data_HPF.(cond{indCond}).trial{tmp_trial}(indRhythmChan,timePointsOffset);
                    
                end
            end
        end
        
        %% save condition-specific output
        
        save([pn.dataOut, IDs{indID}, '_PeriAlpha_Freq.mat'],'PeriEpisodeERP')
        
        
%         figure; 
%         subplot(3,2,1); imagesc(zscore(squeeze(PeriEpisodeERP.trial_onset{indRhythmChan}),[],2),[-3 3])
%         subplot(3,2,2); imagesc(zscore(squeeze(PeriEpisodeERP.trial_offset{indRhythmChan}),[],2),[-3 3])
%         subplot(3,2,3); imagesc(zscore(squeeze(PeriEpisodeERP.trial_onset_notch{indRhythmChan}),[],2),[-3 3])
%         subplot(3,2,4); imagesc(zscore(squeeze(PeriEpisodeERP.trial_offset_notch{indRhythmChan}),[],2),[-3 3])
%         subplot(3,2,5); imagesc(zscore(squeeze(PeriEpisodeERP.trial_onset{indRhythmChan}-PeriEpisodeERP.trial_onset_notch{indRhythmChan}),[],2),[-3 3])
%         subplot(3,2,6); imagesc(zscore(squeeze(PeriEpisodeERP.trial_offset{indRhythmChan}-PeriEpisodeERP.trial_offset_notch{indRhythmChan}),[],2),[-3 3])
%         colormap('gray')
        
%         figure; 
%         subplot(2,2,1); plot(squeeze(nanmean(zscore(squeeze(PeriEpisodeERP.trial_onset{indRhythmChan}),[],2),1)))
%         subplot(2,2,2); plot(squeeze(nanmean(zscore(squeeze(PeriEpisodeERP.trial_offset{indRhythmChan}),[],2),1)))
%         subplot(2,2,3); plot(squeeze(nanmean(zscore(squeeze(PeriEpisodeERP.trial_onset_notch{indRhythmChan}),[],2),1)))
%         subplot(2,2,4); plot(squeeze(nanmean(zscore(squeeze(PeriEpisodeERP.trial_offset_notch{indRhythmChan}),[],2),1)))
%         colormap('gray')

    end

end