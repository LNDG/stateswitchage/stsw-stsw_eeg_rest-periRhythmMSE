function B_runMSE

%SetupIs = 'tardis';
SetupIs = 'local';

if strcmp(SetupIs, 'tardis')
    %pn.root = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S4D_MSE_CSD_v4/';
elseif strcmp(SetupIs, 'local')
    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/';
    addpath([pn.root, 'T_tools/mMSE-master'])
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/T_tools/fieldtrip-20170904'); ft_defaults;
end
pn.dataIn = [pn.root, 'B_data/B_periRhythmOut/'];
pn.dataOut  = [pn.root, 'B_data/C_MSE_Output/']; mkdir(pn.dataOut)


% N = 47 YAs + 52 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for indID = 1:numel(IDs)
    for indCond = 1

        try
        %% select data

        cfg_job.ID = IDs{indID};

        load([pn.dataIn, sprintf('%s_PeriAlpha_Freq.mat', cfg_job.ID)])

        %% adjust input structure

        
        % restructure: the MSE function expects {trials} (here different episodes): channel (here different metrics) + time
        temp1 = cat(1, PeriEpisodeERP.trial_onset{:});
        temp2 = cat(1, PeriEpisodeERP.trial_offset{:});
        temp3 = cat(1, PeriEpisodeERP.trial_onset_notch{:});
        temp4 = cat(1, PeriEpisodeERP.trial_offset_notch{:});
        for indTrial = 1:size(temp1,1)
            PeriEpisodeERP.trial{indTrial}(1,:) = temp1(indTrial,:);
            PeriEpisodeERP.trial{indTrial}(2,:) = temp2(indTrial,:);
            PeriEpisodeERP.trial{indTrial}(3,:) = temp3(indTrial,:);
            PeriEpisodeERP.trial{indTrial}(4,:) = temp4(indTrial,:);
        end                
        PeriEpisodeERP.label{1} = 'PreAlpha';
        PeriEpisodeERP.label{2} = 'PostAlpha';
        PeriEpisodeERP.label{3} = 'PreAlpha_Notch';
        PeriEpisodeERP.label{4} = 'PostAlpha_Notch';
        PeriEpisodeERP.label(5:end) = [];

        PeriEpisodeERP.time = [];
        for indTrial = 1:numel(PeriEpisodeERP.trial)
            PeriEpisodeERP.time{indTrial} = -250/500:1/500:250/500;
        end

        PeriEpisodeERP.sampleinfo = [zeros(size(PeriEpisodeERP.trial,2),1), ...
            repmat(numel(PeriEpisodeERP.time{indTrial}), size(PeriEpisodeERP.trial,2),1)];%PeriEpisodeERP.sampleinfo(1:numel(PeriEpisodeERP.trial),:);

        %% setup mMSE

        % The function expects fieldtrip-style input:
        % data.{trial{},elec,time{},label{},fsample,sampleInfo,trialinfo}

        % within each trial the sorting is channel*trial

        cfg_entropy=[];
        cfg_entropy.toi      = -.25/2:.5/2:.25/2;
        cfg_entropy.timwin  = .25;
        cfg_entropy.m        = 2; % pattern length
        cfg_entropy.r        = 0.5; % similarity criterion
        cfg_entropy.timescales = [1];
        cfg_entropy.freqRequest = (1./cfg_entropy.timescales).*500;
        cfg_entropy.coarsegrainmethod = 'filtskip';
        cfg_entropy.filtmethod = 'lp';
        cfg_entropy.recompute_r  = 'perscale_toi_sp';
        cfg_entropy.polyremoval = 2;
        cfg_entropy.mem_available = 8e9;
        cfg_entropy.allowgpu = 0;

        mse = ft_entropyanalysis(cfg_entropy, PeriEpisodeERP);

        %figure; imagesc(squeeze(mse.permen(1,:,:)))

        %% save output

        save([pn.dataOut, sprintf('%s_MSE_250_OUT.mat', cfg_job.ID)], 'mse', 'cfg_entropy', 'cfg_job')
        catch
        end
    end
end

end % function end