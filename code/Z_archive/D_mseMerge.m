function mseavg = D_mseMerge()

% merge data from alpha-bandstop mMSE analysis

%% set up paths
restoredefaultpath
if ismac
    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/';
else
%     pn.root = '/home/mpib/kosciessa/';
%     backend = 'torque';
end
pn.dataInOUT  = [pn.root, 'B_data/C_MSE_Output/'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% N = 47 YAs + 52 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

nsub = length(IDs);

% load example mse to get nchan etc
subjdir = fullfile(pn.dataInOUT);
cd(subjdir)
runname = sprintf('%s_MSE_OUT.mat', IDs{1}(1:4));
run = dir(runname);
load(run.name)

% JQK: dimord should be: 'chan_scales_time'
ntim = size(mse.sampen,3);
nchan = size(mse.sampen,1);
nscales = size(mse.sampen,2);

mseavg.dat = [];
mseavg.dat = NaN(nsub,nchan,nscales, ntim); % IDs sess channel scale time cond
mseavg.r = NaN(nsub,nchan,nscales, ntim);
mseavg.time = mse.time;
mseavg.scale = mse.timescales;
mseavg.freq = mse.config.freq;

% collect mse results across subjects etc.
for indID = 1:numel(IDs)
    try
        subjdir = fullfile(pn.dataInOUT);
        fprintf('\n\nSubject directory: %s  . . .\n', subjdir)
        fprintf('Loading Subject %s: . . .\n', IDs{indID}(1:4))
        runname = sprintf('%s_MSE_OUT.mat', IDs{indID}(1:4));
        run = dir(runname);
        if isempty(run)
            fprintf('%s not found\n', runname)
            continue
        end
        load(run.name)
        mseavg.dat(indID,:,:,:) = mse.sampen;
        mseavg.r(indID,:,:,:) = mse.r;
    catch
        continue
    end
end

mseavg.IDs = IDs;
mseavg.mse_leg = {'MSEn'};
mseavg.dimord = 'subj_sess_chan_scale_time_cond';
mseavg.dimordsize = size(mseavg.dat);

%% save

save([pn.dataInOUT, 'mseavg_CSD.mat'], 'mseavg');
