function A1_extractPeriRhythmTime

% Note: channels refer to a pair of 'rhythm detected' & the TFR at that
% channel. As the onsets may vary between channels, this means that the TFR
% content may also temporally vary across channels.

%% set up paths

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/';
pn.preprocData  = [pn.root, 'D_eBOSC_noDur/B_data/A_dataPreBOSC/'];
pn.dataBOSC     = [pn.root, 'D_eBOSC_noDur/B_data/B_eBOSCout/'];
pn.dataOut      = [pn.root, 'F_periRhythmMSE/B_data/A_eventTiming/'];

% N = 47 YAs + 52 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for indID = 1:numel(IDs)

    load([pn.preprocData, IDs{indID}, '_eBOSCinput.mat']); % load data (only one needed for time indication)
    
    %% get correct temporal indexing from eBOSC results

    cfg.begsample = find(data.EO.time{1,1}, 1, 'first');
    cfg.endsample = find(data.EO.time{1,1}, 1, 'last');

    time = data.EO.time{1,1};
    time = time(cfg.begsample:cfg.endsample);

    %  parameters for trial padding (same as used for eBOSC)
    
    trialPad = 750;

    %% extract on- and offsets & event durations from rhythmic episodes

    cond = {'EO'};

    freqRanges = [8, 15];
    
    EventOnset_s = cell(1);
    EventOffset_s = cell(1);
    EventDuration_s = cell(1);
    EventAmp = cell(1);
    EventTrial = cell(1);
    
    disp([IDs{indID}]);

    for indFreq = 1:1        
         for indRhythmChan = 1:60 % this is the index for the channel at which rhythmicity is detected
           
            %% load rhythmic episodes from BOSC results
            load([pn.dataBOSC, IDs{indID}, '_eBOSC_Chan',num2str(indRhythmChan),'.mat'],'bosc_episodes'); % load data

            bosc_episodes = bosc_episodes{2}; % only select eyes open condition here
            
            for indEp = 1:size(bosc_episodes,1)
                bosc_episodes{indEp,5} = bosc_episodes{indEp,1}(1,2);
                bosc_episodes{indEp,6} = bosc_episodes{indEp,1}(end,2); 
            end
            
            allEps = bosc_episodes; clear bosc_episodes;
            
            %% extract data from rhythmic episodes
            % Column 4 encodes the duration of rhythmicity in seconds!
            % cycle duration is calculated by multiplying with mean frequency
            CycleDuration = cell2mat(allEps(:,3)).*cell2mat(allEps(:,4));
            criteria = cell2mat(allEps(:,3)) >= freqRanges(indFreq,1) & ...
                cell2mat(allEps(:,3)) <= freqRanges(indFreq,2) & ...
                CycleDuration >= 3;
            EpsInRange = []; EpsInRange = allEps(criteria,:); clear allEps;
            if ~isempty(EpsInRange)
                for indEp = 1:size(EpsInRange,1)
                    % recompute time in original units from episode index
                    EventOnset_s{indFreq, indRhythmChan}(indEp) = time(trialPad+EpsInRange{indEp,1}(1,2));
                    EventOffset_s{indFreq, indRhythmChan}(indEp) = time(trialPad+EpsInRange{indEp,1}(end,2));
                    EventDuration_s{indFreq, indRhythmChan}(indEp) = ...
                        time(trialPad+EpsInRange{indEp,1}(end,2))-time(trialPad+EpsInRange{indEp,1}(1,2));
                    EventAmp{indFreq, indRhythmChan}(indEp) = nanmean(EpsInRange{indEp,2}(:,2));
                end
            end; clear allEps;
         end % detected channel
    end % frequency bin
    
% figure; 
% subplot(1,2,1); histogram(cat(2, EventOnset_s{1,44:60,:}),10)
% subplot(1,2,2); histogram(cat(2, EventOffset_s{1,44:60,:}),10)

save([pn.dataOut, IDs{indID}, '_EpisodeTiming.mat'], 'Event*')

end % subject